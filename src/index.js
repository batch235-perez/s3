// Use the "require" directive to load the express module/package

const express = require("express");


// Create an application using express

const app = express();

// Port to listen to

const port = 4000;


// Setup for allowing the server to handle data from requests
// Allows your app to read json data

app.use(express.json());


// Allow the app to read data from forms
// Applying the option "extended:true" allows us to receive information in other data types such as an object

app.use(express.urlencoded({extended:true}));


// Discussion Session 3 - Unit Testing

let users = [
	{
		name: "Jojo Joestar",
		age: 25,
		username: "Jojo"
	},
	{
		name: "Dio Brando",
		age: 23,
		username: "Dio"
	},
	{
		name: "Jotaro Kujo",
		age: 28,
		username: "Jotaro"
	}
]


// Activity
let artists = [
	{
		name: "Taylor Swift",
		songs: [
			"Cruel Summer",
			"Paper Rings",
			"Cornelia Street"
		] ,
		album: "Lover",
		isActive: true
	},
	{
		name: "Zack Tabudlo",
		songs: [
			"Habang Buhay",
			"Binibini",
			"Calm Me Down"

		] ,
		album: "Episode",
		isActive: true
	},
	{
		name: "Olivia Rodrigo",
		songs: [
			"Traitor",
			"drivers License",
			"Deja Vu"
		] ,
		album: "Sour",
		isActive: true
	}
]


// [SECTION] Routes and Controller for USERS

app.get("/users", (req, res) => {
	console.log(users);
	return res.send(users);
})


app.post("/users", (req, res) => {

	// add simple if statement that if the request body does not have property name, we will send message along with a 400 http status code (Bad Request)
	// hasOwnProperty() returns a boolean if the property name passed exists or does not exist in the given object
	if(!req.body.hasOwnProperty("name")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter NAME"
		})
	}

	if(!req.body.hasOwnProperty("age")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter AGE"
		})
	}
})



// Activity
// [SECTION] Routes and Controller for ARTISTS

app.get("/artists", (req, res) => {
	console.log(artists);
	return res.send(artists);
})


app.post("/artists", (req, res) => {

	if(!req.body.hasOwnProperty("name")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter NAME"
		})
	}

	if(!req.body.hasOwnProperty("songs")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter SONGS"
		})
	}

	if(!req.body.hasOwnProperty("album")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter ALBUM"
		})
	}

	if(!req.body.hasOwnProperty("isActive")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter ISACTIVE"
		})
	}

	if(req.body.isActive == false){
		return res.status(400).send({
			error: "Bad Request - ISACTIVE is FALSE"
		})
	}
})







// Tells our server to listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));




